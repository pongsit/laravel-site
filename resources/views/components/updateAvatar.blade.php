<div class="modal authen_modal fade" id="updateUserAvatar" data-backdrop="static" tabindex="-1" aria-labelledby="loginLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header justify-content-end" style="border-bottom:none;padding:1rem 1rem 0 1rem;">
                <a href="javascript:;" class="close text-secondary" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <div class="modal-body" style="padding:0rem 1rem 1rem 1rem;">
                <h4 class="text-center">เปลี่ยนรูปโปรไฟล์</h4>
                <form class="needs-validation" method="POST" action="profile/avatar/update/{{ Auth::user()->id }}" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="row m-t-20">
                        <div class="form-group col-md-12 mb-4">
                            <label for="formFile" class="form-label"></label>
                            <input class="form-control" type="file" id="formFile" name="avatar">
                        </div>
                        <div class="col-md-12 text-center m-t-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">ยกเลิก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" data-bs-toggle="modal" data-bs-target="#updateUserAvatar">
    Update profile avatar
</button>
  