@extends('hi::layouts.main')

@section('content')
<x-profile::updateAvatar/>
<div class="col-12 col-md-3">
    <div class="d-flex justify-content-center">
        <div class="mt-4 m-2" style="width:150px;height:150px;">
            <div style="
                position: relative;
                background-image: url(profile/show/avatar/{{$avatar ?? '' }}/md); 
                background-repeat:no-repeat; 
                background-size: cover; 
                width:100%;
                padding-top:100%;
                border-radius: 50%;";>
                <div style="position:absolute; bottom:3px;right:3px;font-size:30px; 
                            width:50px; height:50px; padding-left:10px; padding-top:3px; background-color:#ccc" 
                    class="rounded-circle">
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#updateUserAvatar">
                            <i class="fas fa-camera-retro text-secondary"></i>
                        </a>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="text-center m-2">
            
            {{$name ?? '' }}
            {{$username ?? '' }}
            <a href="{{$path_to_core ?? '' }}user/edit.php?id={{$id ?? '' }}">
                <i style="font-size:0.7em;" class="fas fa-pencil-alt"></i> แก้ไข
            </a>
        </div>
    </div>
</div>
<div class="pt-3 pb-3">
    <div>{{$linenotify ?? '' }}</div>
    <div>{{$line_at ?? '' }}</div>
</div>
@stop