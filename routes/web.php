<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Profile\Http\Controllers\ProfileController;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


Route::get('/site', [ProfileController::class, 'index'])->name('profile')->middleware('auth');
Route::get('/site/show/avatar/{name}/{size}', [ProfileController::class, 'showAvatar'])->name('profile.showAvatar');
Route::post('/site/update/avatar/{id}', [ProfileController::class, 'updateAvatar']);