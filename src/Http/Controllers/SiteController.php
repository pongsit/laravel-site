<?php

namespace Pongsit\Site\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;
use App\Models\User;
use Throwable;
use Image;

class SiteController extends Controller
{
    public function index(){
		$user = Auth::user();
		$infos['username'] = $user->username;
		$infos['name'] = $user->name;
		$infos['user_id'] = $user->id;
		$infos['avatar'] = $user->avatar;
		return view('site::index',$infos);
	}

	public function showAvatar($file_name,$size){
		if(empty($size)){
			$size = 'md';
		}
		
		$user = Auth::user();
		if($user->role == 1){
			$user_id = $user->id;
		}
		$path = storage_path('app/public/site/avatar/'.$user_id.'/'.$size.'/'.$file_name);
		dd($path);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
	}

	public function updateAvatar(Request $request){
		$file_name = $request->file('avatar')->hashName();

		$user = Auth::user();
		$user_id = $user->id;
		
		if($user->role =='1' && !empty($request->input('user_id'))){
			$user_id = $request->input('user_id');
		}

		if(Storage::exists('public/site/avatar/'.$user_id)){
            Storage::deleteDirectory('public/site/avatar/'.$user_id);
		}

       	try{
			Storage::put('public/site/avatar/'.$user_id.'/original/',$request->file('avatar'));
			Storage::put('public/site/avatar/'.$user_id.'/sm/',$request->file('avatar'));
			Storage::put('public/site/avatar/'.$user_id.'/md/',$request->file('avatar'));

			$sm_path = storage_path('app/public/site/avatar/'.$user_id.'/sm/'.$file_name);
			$md_path = storage_path('app/public/site/avatar/'.$user_id.'/md/'.$file_name);

			$slider = Image::make($sm_path);
			$slider->fit(150);
			$slider->save();

			$slider = Image::make($md_path);
			$slider->fit(300);
			$slider->save();

		}catch(Throwable $e){
			return back()->withErrors(['Something is wrong! Please try again.']);
		}

		User::where('id', $user_id)->update([
            'avatar' => $file_name
        ]);
		
        return back();
    }
}
